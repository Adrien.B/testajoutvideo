var input = document.querySelector('input');
var preview = document.querySelector('.preview');

input.style.opacity = 0;

input.addEventListener('change', updateImageDisplay);

var fileTypes = [
    'video/webm',
    'video/avi',
    'video/mp4'
  ]

  function returnFileSize(number) {
    if(number < 1024) {
      return number + ' octets';
    } else if(number >= 1024 && number < 1048576) {
      return (number/1024).toFixed(1) + ' Ko';
    } else if(number >= 1048576) {
      return (number/1048576).toFixed(1) + ' Mo';
    }
  }
  
  function validFileType(file) {
    for(var i = 0; i < fileTypes.length; i++) {
      if(file.type === fileTypes[i]) {
        return true;
      }
    }
  
    return false;
  }

function updateImageDisplay() {
    while(preview.firstChild) {
      preview.removeChild(preview.firstChild);
    }
  
    var curFiles = input.files;
    if(curFiles.length === 0) {
      var para = document.createElement('p');
      para.textContent = 'No files currently selected for upload';
      preview.appendChild(para);
    } else {
      var list = document.createElement('ol');
      preview.appendChild(list);
      for(var i = 0; i < curFiles.length; i++) {
        var listItem = document.createElement('li');
        var para = document.createElement('p');
        if(validFileType(curFiles[i])) {
          para.textContent = 'File name ' + curFiles[i].name + ', file size ' + returnFileSize(curFiles[i].size) + '.';
          var video = document.createElement('video');
          video.setAttribute("id", "videoplayer")
          video.src = window.URL.createObjectURL(curFiles[i]);
            Mcorp();
          listItem.appendChild(video);
          listItem.appendChild(para);
  
        } else {
          para.textContent = 'File name ' + curFiles[i].name + ': Not a valid file type. Update your selection.';
          listItem.appendChild(para);
        }
        list.appendChild(listItem);
       
      }
    }
  }

  function Mcorp(){
    var app = MCorp.app("7028818149574465438"); // Initialisation via la clé de l'application
    app.run = function () {
        console.log(MCorp);
        console.log(app.motions);
        app.videosync = MCorp.mediaSync(document.getElementById("videoplayer"), app.motions["motionTest"]); // Association motion et player
        app.motions["motionTest"].on("timeupdate", function (e) {console.log(e.pos);}); // Affichage de la position dans la vidéo à chaque update
        //var sent = app.motions["motionTest"].update(0, 1.0, 0.0); // Lancement de la vidéo
        // (position, vélocité, accélaration)
        

        var buttonLecture = document.createElement("input" ) ;
        buttonLecture.type = "button" ;
        buttonLecture.setAttribute("value", "Lecture");
        buttonLecture.onclick = function(){
          app.motions['motionTest'].update(null, 1, 0.0);
        }

        var buttonPause = document.createElement("input" ) ;
        buttonPause.type = "button" ;
        buttonPause.setAttribute("value", "Pause");
        buttonPause.onclick = function(){
          app.motions['motionTest'].update(null, 0, 0.0);
        }

        var buttonReculer = document.createElement("input" ) ;
        buttonReculer.type = "button" ;
        buttonReculer.setAttribute("value", "Reculer");
        buttonReculer.onclick = function(){
          app.motions['motionTest'].update(app.motions['motionTest'].pos-20, null, 0.0);
        }
        

        var buttonAvancer = document.createElement("input" ) ;
        buttonAvancer.type = "button" ;
        buttonAvancer.setAttribute("value", "Avancer");
        buttonAvancer.onclick = function(){
          app.motions['motionTest'].update(app.motions['motionTest'].pos+20, null, 0.0);
        }
        

        var body = document.getElementsByTagName("body")[0];
        body.appendChild(buttonLecture);
        body.appendChild(buttonPause);
        body.appendChild(buttonReculer);
        body.appendChild(buttonAvancer);
    };
    app.init();
    console.log("app bien initialisee");
    
  }